/**
 * componentExists
 *
 * Check whether the given component exist in either the components or containers directory
 */

const fs = require('fs');
const path = require('path');

const pageStores = fs.readdirSync(path.join(__dirname, '../src/stores'));
const components = pageStores

function componentExists(comp) {
  return components.indexOf(comp) >= 0;
}

module.exports = componentExists;
