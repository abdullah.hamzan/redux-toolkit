

import React from 'react'
import {makeStyles} from "@material-ui/core";
import Select from "react-select"
import { useForm, Controller } from "react-hook-form";
import Input from "@material-ui/core/Input";



const useStyles=makeStyles({
    container:{
        margin:"auto",
        alignItems:"center",
        display:"flex",
        flexDirection:"column",
        
        
    }
})

const RegisterPage = () => {
    const classes=useStyles

    const { control, handleSubmit } = useForm({
      defaultValues: {
        firstName: '',
        select: {}
      }
    });
    const onSubmit = data => console.log(data);
  return (
    <div className={classes.container}>
 <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="firstName"
        control={control}
        render={({ field }) => <Input {...field} />}
      />
      <Controller
       
        name="select"
        control={control}
        render={({ field }) => <Select 

        isMulti
          {...field} 
          options={[
            { value: "chocolate", label: "Chocolate" },
            { value: "strawberry", label: "Strawberry" },
            { value: "vanilla", label: "Vanilla" }
          ]} 
        />}
      />
      <input type="submit" />
    </form>
  
    </div>
  )
}

export default RegisterPage