

import React,{useState,useEffect} from 'react'

import {useSelector,useDispatch} from "react-redux";
import { getProducts } from '../redux/productSlice';

import { Button, CircularProgress, makeStyles, TextField, Typography } from '@material-ui/core';
import { getPostsData } from '../redux/postsSlice';
import {authLoginData} from "../redux/loginSlice";

// import { update } from '../redux/productSlice';



const useStyles=makeStyles({

  title:{

    fontSize:"20px",
    fontWeight:"bold",
    marginTop:"20px",
    marginBottom:"20px",
    textAlign:"center"

  },

  card:{
    display:"flex",
    flexDirection:"row",
    justifyContent:"center",
    alignItems:"center",
    "& .items":{

      display:"flex",

      flexDirection:"column",
      padding:"10x",
      width:"400px",
      borderRadius:"10px",
      boxShadow:"border-box",
     
    }
  

    }



})



const HomePage = () => {

  const classes=useStyles();

  const dispatch=useDispatch();

  const {products,isLoading,error}=useSelector(state=>state.product);

  const {posts,isLoading:isLoadingPost,error:errorPost}=useSelector(state=>state.posts)
  const {result,isLoading:isLoadingAuth,error:errorAuth,isOpen}=useSelector(state=>state.auth)

// console.log("dataStore:",products,isLoading,error);

console.log("dataStoreAuth",result,isLoadingAuth,errorAuth,isOpen);

// console.log("dataStorePosts:",posts,isLoadingPost,errorPost)

  const [formData,setFormData]=useState({
    title:"",
    price:""
  });

  const [formLogin,setFormLogin]=useState({
    email:"",
    password:""
  });


  useEffect(()=>{

    dispatch(getProducts())
    dispatch(getPostsData())


  },[])


  console.log("fromData:",formData)



  const onSubmitHandler=(e)=>{
    e.preventDefault();



    console.log("hello world")

    console.log("formData:",formData)
    // dispatch(update(formData))





  }

  const onLoginHandler=()=>{

    console.log(formLogin);

    dispatch(authLoginData(formLogin));
  }
  return (
    <div>

      <div style={{
        width:"400px",
        height:"100px",
        borderRadius:"5px",
        
      }}>
        {
          isLoading ? (
            <div>
              <CircularProgress />
              </div>
          ):(
            <div>

        <h3>Title: dummny title</h3>
        <h4>Price: dummy price</h4>

            </div>
          )
        }


      </div>


      <form onSubmit={onSubmitHandler}>
        <input  name='title' value={formData?.title} placeholder="input type title" onChange={(e)=>setFormData({...formData,title:e.target.value})} /><br />
        <input name="price" value={formData?.price} placeholder="input type price" onChange={(e)=>setFormData({...formData,price:e.target.value})} /><br />
        <input style={{
           backgroundColor:"red",
           cursor:"pointer",
           width:"60px",
           height:"24px"
        }} value="Update" type="submit" />


      </form>

      <hr style={{
        marginTop:"20px",
        marginBottom:"10px",
      }} />

      <Typography className={classes.title}>Tugas Redux Tool kit </Typography>

      <div className={classes.card}>

        <div className="items">

        <TextField  label="email" type="email" variant="outlined" value={formLogin.email} onChange={(e)=>setFormLogin({...formLogin,email:e.target.value})} />
      <br />

      <TextField  label="password" type="password" variant="outlined" value={formLogin.password} onChange={(e)=>setFormLogin({...formLogin,password:e.target.value})} />

      <br />
      <Button variant="contained" color="primary" onClick={onLoginHandler}>
        {isOpen ? "Loading...":"Login"}
      </Button>

        </div>

      </div>

 

      
    </div>
  )
}

export default HomePage
