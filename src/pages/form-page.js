

import { TextField,makeStyles,Button } from '@material-ui/core'
import { useForm, Controller } from "react-hook-form";

import React,{useEffect} from 'react';
import { addPostsData } from '../redux/postsSlice';
import { useDispatch,useSelector } from 'react-redux';
import Swal from 'sweetalert2';


const useStyles=makeStyles({
    page:{
        display:"flex",
        justifyContent:"center",
        alignItems:"center",
        flexDirection:"row"
    }
})

const FormPage = () => {
    const classes=useStyles();
    const dispatch=useDispatch();
    const {posts,isLoading:isLoadingPost,error:errorPost,isOpen}=useSelector(state=>state.posts)

    useEffect(()=>{



        if(isOpen){

            Swal.fire(
                'Success Add Data',
                'You clicked the button!',
                'success'
              ).then(res=>{

                console.log("res:",res);
              })
        }
        



    },[isOpen])


    const { control, handleSubmit } = useForm({
        defaultValues: {
          title: '',
          body: ""
        }
      });

const onSubmit = data => {
    console.log(data);

    dispatch(addPostsData(data));

    


};
  return (
    <>

    <div className={classes.page}>

    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="title"
        control={control}
        render={({ field }) => <TextField {...field} label="title" variant="outlined" />}
      />
      <br />
      <br />
      <Controller
        name="body"
        control={control}
        render={({ field }) => <TextField {...field} label="body" variant="outlined" />
    
    
    }
      />
      <br />
      <br />
      <br />
      <Button type="submit" variant="contained" color="primary">
       Save
      </Button>
    </form>
    </div>
    </>
  )
}

export default FormPage
