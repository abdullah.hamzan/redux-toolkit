
import React,{useEffect} from 'react'
import { useNavigate,useParams,Outlet } from 'react-router-dom'

const DetailPage = () => {
    const navigate=useNavigate();
    const {id}=useParams();

    // check params throw is integer
    const isInteger=/^-?[0-9]+$/.test(id+'');
    useEffect(()=>{

        if(!isInteger){
            navigate("/")
        }

    },[])
  return (
    <div>
        Detail page id :{id}
        <br />
        <Outlet />
    </div>
  )
}

export default DetailPage