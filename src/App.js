import React from 'react'
import { Link, Route, Routes } from 'react-router-dom'
import ChildDetaiPage from './pages/child-detail-page'
import DetailPage from './pages/detail-page'
import FormPage from './pages/form-page'
import HomePage from './pages/home-page'
import NotFoundPage from './pages/not-found-page'
import RegisterPage from './pages/register-page'
import SomePage from './pages/some-page'



const App = () => {
  return (
    <div>
      <nav>
        <Link to="/">Home</Link>
        <Link to="/some" style={{marginLeft:10}}>Some</Link>
        <Link to="/register" style={{marginLeft:10}}>Register</Link>
        <Link to="/form" style={{marginLeft:10}}>Form</Link>
      </nav>
      <hr />
      <Routes>

        <Route path="/" element={<HomePage />} />




        <Route path="some" element={<SomePage />} />
        <Route path="register" element={<RegisterPage />} />
        <Route path="form" element={<FormPage />} />
        <Route path="/:id" element={<DetailPage />}>
          {/* <Route index element={<DetailPage />} /> */}
          <Route path="info" element={<ChildDetaiPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Route>
      </Routes>
      
    </div>
  )
}

export default App
