
import {createSlice,createAsyncThunk,createEntityAdapter} from "@reduxjs/toolkit"
import axios from "axios";

const initialState={
    isLoading:false,
    products:[],
    error:{

    }
}




export const getProducts=createAsyncThunk("products/getProducts",async ()=>{
    const res= await axios.get("http://localhost:5000/products"
    );
    return res?.data;

});




const productSlice=createSlice({
    name:"product",
    initialState:initialState,
    extraReducers:builder=>{
        
        builder.addCase(getProducts.pending,state=>{
            state.isLoading=true;

        })

        builder.addCase(getProducts.fulfilled,(state,action)=>{

            state.isLoading=true;
            state.error={};
            state.products=action.payload;


        })
        builder.addCase(getProducts.rejected,(state,action)=>{
            state.isLoading=false;
            state.products=[];
            state.error=action?.error?.message;
        })

    }

    
});

// export const {getProducts}=productSlice.actions;

export default productSlice.reducer;