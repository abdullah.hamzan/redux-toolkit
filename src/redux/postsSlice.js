
import {createSlice,createAsyncThunk,createEntityAdapter} from "@reduxjs/toolkit"
import axios from "axios";

const initialState={
    isLoading:false,
    posts:[],
    isOpen:false,
    result:{},
    error:{

    }
}


export const getPostsData=createAsyncThunk("posts/getPosts",async ()=>{
    const res= await axios.get("https://jsonplaceholder.typicode.com/posts"
    );
    return res?.data;

});

export const addPostsData=createAsyncThunk("posts/addPosts",async (payload)=>{
    const res= await axios.post("https://jsonplaceholder.typicode.com/posts",payload
    );
    return res?.data;

});




const postsSlice=createSlice({
    name:"product",
    initialState:initialState,
    extraReducers:builder=>{
        
        builder.addCase(getPostsData.pending,state=>{
            state.isLoading=true;

        })

        builder.addCase(getPostsData.fulfilled,(state,action)=>{

            state.isLoading=true;
            state.error={};
            state.posts=action.payload;


        })
        builder.addCase(getPostsData.rejected,(state,action)=>{
            state.isLoading=false;
            state.posts=[];
            state.error=action?.error?.message;
        })

        builder.addCase(addPostsData.pending,state=>{
            state.isLoading=true;
        })

        builder.addCase(addPostsData.fulfilled,(state,action)=>{
            state.isLoading=false;
            state.result=action.payload;
            state.isOpen=true;
        })

        builder.addCase(addPostsData.rejected,(state,action)=>{
            state.isLoading=false;
            state.posts=[];
            state.error=action?.error?.message;
        })


    }

    
});



export default postsSlice.reducer  ;