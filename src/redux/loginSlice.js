
import {createSlice,createAsyncThunk,createEntityAdapter} from "@reduxjs/toolkit"
import axios from "axios";

const initialState={
    isLoading:false,
    isOpen:false,
    result:{},
    error:{

    }
}


export const authLoginData=createAsyncThunk("auth/login",async (payload)=>{
    const res= await axios.post("http://10.194.33.13:8090/api/login/token",payload
    );
    return res?.data;

});





const loginSlice=createSlice({
    name:"login",
    initialState:initialState,
    extraReducers:builder=>{
        
        builder.addCase(authLoginData.pending,state=>{
            state.isLoading=true;
            state.isOpen=true;

        })

        builder.addCase(authLoginData.fulfilled,(state,action)=>{

            state.isLoading=false;
            state.isOpen=false;
            state.error={};
            state.result=action.payload;


        })
        builder.addCase(authLoginData.rejected,(state,action)=>{
            state.isLoading=false;
            state.isOpen=false;
            state.error=action?.error?.message;
        })



    }

    
});



export default loginSlice.reducer  ;