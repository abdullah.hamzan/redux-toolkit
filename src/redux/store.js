
import {configureStore} from "@reduxjs/toolkit"
import postsSlice from "./postsSlice";
import loginSlice from"./loginSlice";



import productSlice from "./productSlice";

export const store= configureStore({
    reducer:{
        product:productSlice,
        posts:postsSlice,
        auth:loginSlice

    }
});

