// products Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = 'products/SET_LOADING';
export const CLEAR_ERROR = 'products/CLEAR_ERROR';
export const SET_ERROR = 'products/SET_ERROR';
export const INIT_DATA = 'products/INIT_DATA';
export const SET_DOUBLE_SUBMIT = 'products/SET_DOUBLE_SUBMIT';

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
})

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});
