// categori Actions
// --------------------------------------------------------

/* eslint-disable space-before-function-paren */
export const SET_LOADING = 'categori/SET_LOADING';
export const CLEAR_ERROR = 'categori/CLEAR_ERROR';
export const SET_ERROR = 'categori/SET_ERROR';
export const INIT_DATA = 'categori/INIT_DATA';
export const SET_DOUBLE_SUBMIT = 'categori/SET_DOUBLE_SUBMIT';

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
})

export const setDoubleSubmit = (payload) => ({
  type: SET_DOUBLE_SUBMIT,
  payload,
});
