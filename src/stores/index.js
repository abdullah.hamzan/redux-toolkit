import {createStore,applyMiddleware,compose} from "redux";
import rootReducer from "./reducers";
import thunk from "redux-thunk";



let store=createStore(rootReducer);


export default store;
